import React from "react";
import * as Facebook from "expo-facebook";
import * as Sentry from "sentry-expo";
import { auth, db, facebook } from "../utils/firebase";
import {
  _readAsyncStorage,
  _writeAsyncStorage,
  _clearAsyncStorage
} from "../utils/storage";
import { FACEBOOK_APP_ID } from "../utils/constants";

class AuthService {
  getCurrentUser = () => {
    const user = auth.currentUser;
    return user;
  };

  signInOrSignUpWithFacebook = async () => {
    try {
      await Facebook.initializeAsync(FACEBOOK_APP_ID);

      const { type, token } = await Facebook.logInWithReadPermissionsAsync(
        FACEBOOK_APP_ID,
        {
          permissions: ["public_profile", "email"],
          behavior: "native"
        }
      );

      if (type === "success") {
        let credential = await facebook.credential(null, token);
        const { user } = await auth.signInWithCredential(credential);

        if (user) {
          await _writeAsyncStorage("token", token);

          const userRef = db.collection("users").doc(user.uid);

          userRef
            .get()
            .then(userDoc => {
              if (!userDoc.exists) {
                const newUser = userRef.set({
                  id: user.uid,
                  name: user.displayName,
                  picture: user.photoURL,
                  email: user.email
                });

                if (newUser) {
                  _writeAsyncStorage("user", user.uid);
                  console.log("A felhasználót létrehoztuk: ", user.uid);

                  return newUser;
                }
              } else {
                _writeAsyncStorage("user", user.uid);
                console.log("A felhasználó már létezik: ", user.displayName);
              }
            })
            .catch(error => {
              alert(error);
              Sentry.captureException(error);
            });
        }
      } else {
        alert(`sign in ${type}`);
      }
    } catch (error) {
      alert(error);
      Sentry.captureException(error);
    }
  };

  signOut = async () => {
    try {
      await auth.signOut();
      await _clearAsyncStorage();
    } catch (error) {
      Sentry.captureException(error);
    }
  };

  deleteUser = async () => {
    try {
      const user = await auth.currentUser;

      if (user) {
        const userRef = db.collection("users").doc(user.uid);

        userRef
          .get()
          .then(userDoc => {
            if (userDoc.exists) {
              userRef
                .delete()
                .then(async () => {
                  try {
                    await user.delete();
                    console.log("Document successfully deleted!");
                  } catch (error) {
                    console.error("Error removing document: ", error);
                    Sentry.captureException(error);
                  }
                })
                .catch(error => {
                  console.error("Error removing document: ", error);
                  Sentry.captureException(error);
                });
            } else {
              _writeAsyncStorage("user", user.uid);
              console.log("A felhasználó már létezik: ", user.displayName);
            }
          })
          .catch(error => {
            alert(error);
            Sentry.captureException(error);
          });
      }
    } catch (error) {
      console.log(error);
      Sentry.captureException(error);
    }
  };
}

export default new AuthService();
