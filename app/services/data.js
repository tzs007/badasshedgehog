import * as Sentry from "sentry-expo";
import { db, storage } from "../utils/firebase";

class DataService {
  getCharacters = async () => {
    try {
      const snapshot = await db.collection("characters").get();

      return Promise.all(
        snapshot.docs.map(async doc => {
          const slug = doc.data().name;
          const defaultImage = await storage.getImage(
            "characters",
            "default.png",
            slug
          );

          if (defaultImage) {
            return {
              id: doc.id,
              defaultImage,
              ...doc.data()
            };
          } else {
            console.log("valami elakadt");
          }
        })
      );
    } catch (error) {
      Sentry.captureException(error);
      console.log(`getCharacters error: `, error);
    }
  };

  getPranks = async () => {
    try {
      const snapshot = await db
        .collection("pranks")
        .orderBy("isPremium", "asc")
        .get();

      return snapshot.docs.map(doc => ({ id: doc.id, ...doc.data() }));
    } catch (error) {
      Sentry.captureException(error);
      console.log(`getPranks error: `, error);
    }
  };

  getPages = async () => {
    try {
      const snapshot = await db
        .collection("pages")
        .where("isPublished", "==", true)
        .get();
      return snapshot.docs.map(doc => ({ id: doc.id, ...doc.data() }));
    } catch (error) {
      Sentry.captureException(error);
      console.log(`getPages error: `, error);
    }
  };
}

export default new DataService();
