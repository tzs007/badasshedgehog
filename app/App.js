import React, { Component } from "react";
import * as Sentry from "sentry-expo";
import * as Font from "expo-font";
import { AppLoading } from "expo";
import { Asset } from "expo-asset";
import { Ionicons } from "@expo/vector-icons";
import Routes from "./router/Routes";
import StoreProvider from "./store/provider";
import AuthService from "./services/auth";
import DataService from "./services/data";
import { _readAsyncStorage } from "./utils/storage";
import { StyleProvider } from "native-base";
import getTheme from "./native-base-theme/components";
import vuduStyle from "./native-base-theme/variables/vuduboss";
import { auth, storage } from "./utils/firebase";

import icon from "./assets/images/icon.png";
import splash from "./assets/images/splash.png";

Sentry.init({
  dsn: "https://02e0ee6fae88461a879212f505606234@sentry.io/2981201",
  enableInExpoDevelopment: true,
  debug: true
});

class App extends Component {
  constructor(props) {
    super(props);

    this.state = {
      isFontLoaded: false,
      isAppReady: false,
      isAuthChecked: false,

      // push to context
      payload: {
        isLoggedIn: false,
        user: this._initialUser(),
        prankee: this._initialPrankee(),
        pranks: null,
        characters: null,
        resetCharacterCarousel: false,
        pages: null
      },
      actions: {
        signIn: this.signIn,
        signOut: this.signOut,
        setPrankee: this.setPrankee,
        resetPrankee: this.resetPrankee,
        deleteProfile: this.deleteProfile
      }
    };
  }

  componentDidMount = async () => {
    try {
      await this.getCharacters();
      await this.getPranks();
      await this.getPages();

      await Font.loadAsync({
        "nunito-regular": require("./assets/fonts/Nunito/Nunito-Regular.ttf"),
        "nunito-extra-bold": require("./assets/fonts/Nunito/Nunito-ExtraBold.ttf"),
        ...Ionicons.font
      });

      this.setState({
        isFontLoaded: true
      });
    } catch (error) {
      Sentry.captureException(error);
    }
  };

  _initialUser = () => ({
    displayName: "",
    email: "",
    photoURL: ""
  });

  _initialPrankee = async () => {
    try {
      const characters = await DataService.getCharacters();

      if (characters) {
        return {
          name: "",
          character: characters[0].name,
          defaultImage: characters[0].defaultImage
        };
      } else {
        console.log("error in getCharacter when initialPrankee called");
      }
    } catch (error) {
      console.log(error);
      Sentry.captureException(error);
    }
  };

  _auth = auth.onAuthStateChanged(user => {
    const { payload } = this.state;
    if (user) {
      const { displayName, email, photoURL } = user;
      payload.isLoggedIn = true;
      payload.user.displayName = displayName;
      payload.user.email = email;
      payload.user.photoURL = photoURL;
    } else {
      payload.isLoggedIn = false;
      payload.user = this._initialUser();
    }
    this.setState({ isAuthChecked: true, payload });
    this._auth();
  });

  _cacheResourceAsync = async () => {
    const images = [icon, splash];

    const cacheImages = images.map(image =>
      Asset.fromModule(image).downloadAsync()
    );

    return Promise.all(cacheImages);
  };

  _setAppReady = () => {
    this.setState({
      isAppReady: true
    });
  };

  // AUTH

  signIn = async () => {
    try {
      await AuthService.signInOrSignUpWithFacebook();
      const token = await _readAsyncStorage("token");
      const { displayName, email, photoURL } = AuthService.getCurrentUser();

      if (token && email) {
        const { payload } = this.state;
        payload.isLoggedIn = true;
        payload.user.displayName = displayName;
        payload.user.email = email;
        payload.user.photoURL = photoURL;

        this.setState({
          payload
        });
      }
    } catch (error) {
      Sentry.captureException(error);
      console.log(error);
    }
  };

  signOut = async () => {
    try {
      await AuthService.signOut();
      const token = await _readAsyncStorage("token");
      if (!token) {
        const { payload } = this.state;
        payload.isLoggedIn = false;

        this.setState({
          payload
        });
      }
    } catch (error) {
      Sentry.captureException(error);
    }
  };

  getPranks = async () => {
    try {
      const pranks = await DataService.getPranks();

      if (pranks) {
        const { payload } = this.state;
        payload.pranks = pranks;

        this.setState({
          payload
        });
      } else {
        console.log("prank:", pranks);
      }
    } catch (error) {
      console.log("getPranks: ", error);
      Sentry.captureException(error);
    }
  };

  getCharacters = async () => {
    try {
      const characters = await DataService.getCharacters();

      if (characters) {
        const { payload } = this.state;
        payload.characters = characters;
        payload.prankee.character = characters[0].name;
        payload.prankee.defaultImage = characters[0].defaultImage;

        this.setState({
          payload
        });
      } else {
        console.log("characters:", characters);
      }
    } catch (error) {
      console.log("getCharacters: ", error);
      Sentry.captureException(error);
    }
  };

  // PRANKEE

  setPrankee = (value, isCharacter = false) => {
    const { payload } = this.state;
    const character = payload.characters.filter(
      character => character.name === value
    );

    if (isCharacter) {
      payload.prankee.character = value;
      payload.prankee.defaultImage = character[0].defaultImage;
    } else {
      payload.prankee.name = value;
    }

    payload.resetCharacterCarousel = false;

    this.setState({
      payload
    });
  };

  resetPrankee = () => {
    const { payload } = this.state;
    payload.prankee = this._initialPrankee();
    payload.resetCharacterCarousel = true;

    this.setState({
      payload
    });
  };

  // CONTENT

  getPages = async () => {
    try {
      const pages = await DataService.getPages();

      if (pages) {
        const { payload } = this.state;
        payload.pages = pages;

        this.setState({
          payload
        });
      } else {
        console.log("oldalak:", pages);
      }
    } catch (error) {
      console.log("getPages: ", error);
      Sentry.captureException(error);
    }
  };

  deleteProfile = async () => {
    AuthService.deleteUser();
    await this.signOut();
  };

  componentDidCatch(error, info) {
    Sentry.captureException(error);
  }

  render() {
    const {
      isFontLoaded,
      isAppReady,
      isAuthChecked,
      payload,
      actions
    } = this.state;

    return (
      <>
        {isFontLoaded && isAppReady && isAuthChecked && payload ? (
          <StoreProvider actions={actions} payload={payload}>
            <StyleProvider style={getTheme(vuduStyle)}>
              <Routes />
            </StyleProvider>
          </StoreProvider>
        ) : (
          <AppLoading
            startAsync={this._cacheResourceAsync}
            onFinish={this._setAppReady}
            onError={console.warn}
          />
        )}
      </>
    );
  }
}

export default App;
