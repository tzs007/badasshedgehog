import React, { Component } from "react";
import { View, Text, StatusBar, StyleSheet } from "react-native";
import { Container } from "native-base";
import vuduStyle from "../../native-base-theme/variables/vuduboss";

const Master = ({ children }) => (
  <>
    <StatusBar backgroundColor="white" barStyle="dark-content" />
    <Container style={styles.container}>{children}</Container>
  </>
);

const styles = StyleSheet.create({
  container: {
    marginTop: 30
  }
});

export default Master;
