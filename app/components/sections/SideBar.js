import React, { Component } from "react";
import { Platform, View, Text, Image, StyleSheet } from "react-native";
import { TouchableOpacity } from "react-native-gesture-handler";
import { Icon } from "native-base";
import { DrawerContentScrollView } from "@react-navigation/drawer";
import Store from "../../store";
import vuduStyle from "../../native-base-theme/variables/vuduboss";

import sidebarLogo from "../../assets/images/sidebar-logo.png";

class SideBar extends Component {
  constructor(props) {
    super(props);
  }

  render() {
    const { signOut } = this.context.actions;
    const { navigation } = this.props;

    return (
      <DrawerContentScrollView {...this.props}>
        <View style={styles.sidebarHeader}>
          <View style={styles.sidebarLogoWrapper}>
            <Image source={sidebarLogo} style={styles.sidebarLogo} />
          </View>
          <View>
            <TouchableOpacity
              style={styles.drawerClose}
              onPress={() => navigation.closeDrawer()}
            >
              <Icon name="close" />
            </TouchableOpacity>
          </View>
        </View>
        <TouchableOpacity
          style={styles.drawerItem}
          onPress={() => navigation.navigate("WhosGetPrankScreen")}
        >
          <Icon
            type="FontAwesome5"
            name="laugh-squint"
            style={styles.drawerIcon}
          />
          <Text style={styles.drawerLabel}>Let’s Prank</Text>
        </TouchableOpacity>
        <TouchableOpacity
          style={styles.drawerItem}
          onPress={() => navigation.navigate("ProfileScreen")}
        >
          <Icon type="FontAwesome" name="user-o" style={styles.drawerIcon} />
          <Text style={styles.drawerLabel}>Profile</Text>
        </TouchableOpacity>
        {Platform.OS === "ios" && (
          <TouchableOpacity
            style={styles.drawerItem}
            onPress={() => navigation.navigate("PurchasePremiumPranksScreen")}
          >
            <Icon
              type="FontAwesome"
              name="credit-card"
              style={styles.drawerIcon}
            />
            <Text style={styles.drawerLabel}>Purchase Premium Pranks</Text>
          </TouchableOpacity>
        )}
        {/*         <TouchableOpacity
          style={styles.drawerItem}
          onPress={() => navigation.navigate('MyRankScreen')}
        >
          <Icon type="FontAwesome" name="trophy" style={styles.drawerIcon} />
          <Text style={styles.drawerLabel}>My Rank</Text>
        </TouchableOpacity> */}
        <TouchableOpacity
          style={styles.drawerItem}
          onPress={() => navigation.navigate("AboutGameScreen")}
        >
          <Icon type="FontAwesome" name="gamepad" style={styles.drawerIcon} />
          <Text style={styles.drawerLabel}>About Game</Text>
        </TouchableOpacity>
        <TouchableOpacity
          style={styles.drawerItem}
          onPress={() => navigation.navigate("TermsAndConditionsScreen")}
        >
          <Icon
            type="FontAwesome"
            name="file-text-o"
            style={styles.drawerIcon}
          />
          <Text style={styles.drawerLabel}>Terms & Conditions</Text>
        </TouchableOpacity>
        <TouchableOpacity
          style={styles.drawerItem}
          onPress={() => navigation.navigate("PrivacyPolicyScreen")}
        >
          <Icon
            type="FontAwesome"
            name="user-secret"
            style={styles.drawerIcon}
          />
          <Text style={styles.drawerLabel}>Privacy Policy</Text>
        </TouchableOpacity>
        <TouchableOpacity style={styles.drawerItem} onPress={() => signOut()}>
          <Icon type="FontAwesome" name="power-off" style={styles.drawerIcon} />
          <Text style={styles.drawerLabel}>Logout</Text>
        </TouchableOpacity>
      </DrawerContentScrollView>
    );
  }
}

const styles = StyleSheet.create({
  sidebarHeader: {
    flexDirection: "row",
    justifyContent: "space-between",
    alignItems: "center",
    paddingHorizontal: 20,
    paddingVertical: 25
  },
  sidebarLogo: {
    width: 130,
    height: 41
  },
  drawerItem: {
    borderRadius: 0,
    paddingHorizontal: 20,
    paddingVertical: 10,
    marginVertical: 0,
    borderBottomWidth: 1,
    borderBottomColor: "#dadada",
    flexDirection: "row",
    justifyContent: "flex-start",
    alignItems: "center"
  },
  drawerLabel: {
    fontFamily: vuduStyle.fontFamilyExtraBold,
    marginLeft: 10,
    color: vuduStyle.brandDark
  },
  drawerIcon: {
    color: vuduStyle.brandDark,
    width: 30
  },
  drawerClose: {
    justifyContent: "center",
    alignItems: "center",
    width: 40,
    height: 40
  }
});

SideBar.contextType = Store;

export default SideBar;
