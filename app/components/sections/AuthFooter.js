import React from "react";
import { View, StyleSheet } from "react-native";
import { Button, Text } from "native-base";
import { useNavigation } from "@react-navigation/native";
import vuduStyle from "../../native-base-theme/variables/vuduboss";

const AuthFooter = ({ navigation }) => {
  return (
    <View style={styles.footer}>
      <View style={styles.footerTab}>
        <Button
          small
          transparent
          onPress={() => navigation.navigate("TermsAndConditionsScreen")}
        >
          <Text style={styles.buttonText}>Terms</Text>
        </Button>
      </View>
      <View style={styles.footerTab}>
        <Button
          small
          transparent
          onPress={() => navigation.navigate("PrivacyPolicyScreen")}
        >
          <Text style={styles.buttonText}>Privacy</Text>
        </Button>
      </View>
      <View style={styles.footerTab}>
        <Button
          small
          transparent
          onPress={() => navigation.navigate("AboutGameScreen")}
        >
          <Text style={styles.buttonText}>About Game</Text>
        </Button>
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  footer: {
    flexDirection: "row",
    justifyContent: "space-between",
    alignItems: "center",
    width: "100%"
  },
  footerTab: {
    //flex: 1,
    marginHorizontal: 5,
    paddingVertical: 10
  },
  buttonText: {
    color: vuduStyle.brandDark
  }
});

export default props => {
  const navigation = useNavigation();
  return <AuthFooter {...props} navigation={navigation} />;
};
