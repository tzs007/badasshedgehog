import React, { Component } from "react";
import { View, StyleSheet, Dimensions, Image } from "react-native";
import { Card, CardItem, Spinner } from "native-base";
import Carousel from "react-native-snap-carousel";
import Store from "../../store";
import vuduStyle from "../../native-base-theme/variables/vuduboss";

const marginHorizontal = 20 * 2;
const deviceWidth = Dimensions.get("window").width;
const deviceHeight = Dimensions.get("window").height;

const itemWidth = deviceWidth - marginHorizontal;
const itemHeight = deviceHeight - 265; // 265 = header heigth + footer height + paddingVerticals on WhosGetPrankScreen

class CharacterCarousel extends Component {
  constructor(props) {
    super(props);
  }

  _renderItem = ({ item, index }) => {
    return (
      <Card style={styles.carouselCard} key={`card-${index}`}>
        <CardItem cardBody>
          <View style={styles.carouselImageWrapper}>
            <Image
              source={{ uri: item.defaultImage }}
              style={styles.carouselImage}
            />
          </View>
        </CardItem>
      </Card>
    );
  };

  componentDidUpdate(prevProps, prevState) {
    const { resetCharacterCarousel } = this.context.payload;

    if (this._carousel && resetCharacterCarousel) {
      this._carousel.snapToItem(0);
    }
  }

  render() {
    const { characters } = this.context.payload;
    const { setPrankee } = this.context.actions;

    return characters ? (
      <Carousel
        layout="default"
        data={characters}
        ref={c => {
          this._carousel = c;
        }}
        renderItem={this._renderItem}
        sliderWidth={deviceWidth}
        itemWidth={itemWidth}
        containerCustomStyle={styles.carousel}
        onSnapToItem={index => setPrankee(characters[index].name, true)}
        inactiveSlideOpacity={0.2}
        inactiveSlideScale={0.96}
      />
    ) : (
      <Spinner color={vuduStyle.brandPrimary} />
    );
  }
}

const styles = StyleSheet.create({
  carousel: {
    flex: 1
  },
  carouselCard: {
    borderRadius: 20,
    overflow: "hidden"
  },
  carouselImageWrapper: {
    flex: 1,
    flexDirection: "column",
    backgroundColor: vuduStyle.brandWarning,
    width: itemWidth,
    height: itemHeight,
    justifyContent: "center",
    alignItems: "center"
  },
  carouselImage: {
    flex: 1,
    width: itemWidth - marginHorizontal
  }
});

CharacterCarousel.contextType = Store;

export default CharacterCarousel;
