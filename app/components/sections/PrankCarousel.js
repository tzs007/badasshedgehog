import React, { Component } from "react";
import { Platform, View, ScrollView, StyleSheet, Image } from "react-native";
import { TouchableOpacity } from "react-native-gesture-handler";
import { useNavigation } from "@react-navigation/native";
import { Badge, Icon, Text } from "native-base";
import Store from "../../store";
import vuduStyle from "../../native-base-theme/variables/vuduboss";

import boomTrah from "../../assets/lottie/boom-trah.json";
import explodingHeart from "../../assets/lottie/exploding-heart.json";

class PrankCarousel extends Component {
  constructor(props) {
    super(props);
  }

  render() {
    const { handlePurchase, handlePrank } = this.props;
    const { pranks } = this.context.payload;

    // console.log("pranks in carousel", pranks);

    return (
      <ScrollView horizontal>
        <View style={styles.prankList}>
          {pranks &&
            pranks.map((prank, index) => (
              <View key={`prank-${index}`} style={styles.prankListItem}>
                <TouchableOpacity
                  onPress={
                    prank.isPremium && Platform.OS === "ios"
                      ? handlePurchase
                      : () =>
                          handlePrank(
                            prank.isKind ? explodingHeart : boomTrah,
                            prank.type
                          )
                  }
                >
                  <>
                    <View style={styles.prankListItemIconWrapper}>
                      {prank.isPremium && Platform.OS === "ios" && (
                        <Badge
                          style={styles.prankListItemUnlockPremium}
                          primary
                        >
                          <Icon
                            type="FontAwesome5"
                            name="dollar-sign"
                            style={{ fontSize: 12 }}
                          />
                        </Badge>
                      )}
                      <Image
                        source={{ uri: prank.icon }}
                        style={styles.prankListItemIcon}
                      />
                    </View>
                    <Text style={styles.prankListItemText}>{prank.name}</Text>
                  </>
                </TouchableOpacity>
              </View>
            ))}
        </View>
      </ScrollView>
    );
  }
}

const styles = StyleSheet.create({
  prankList: {
    flex: 1,
    paddingHorizontal: 15,
    flexDirection: "row",
    justifyContent: "space-between"
  },
  prankListItem: {
    width: 80,
    paddingHorizontal: 5
  },
  prankListItemIconWrapper: {
    width: 70,
    height: 70,
    overflow: "hidden",
    justifyContent: "center",
    alignItems: "center",
    borderRadius: 20,
    backgroundColor: "#F1F3F6"
  },
  prankListItemIcon: {
    width: 60,
    height: 60
  },
  prankListItemText: {
    fontFamily: vuduStyle.fontFamilyRegular,
    fontSize: 10,
    textAlign: "center",
    paddingTop: 10
  },
  prankListItemUnlockPremium: {
    position: "absolute",
    zIndex: 99,
    right: 0,
    top: 0,
    justifyContent: "center",
    alignItems: "center",
    width: 20,
    height: 20
  }
});

PrankCarousel.contextType = Store;

export default props => {
  const navigation = useNavigation();
  return <PrankCarousel {...props} navigation={navigation} />;
};
