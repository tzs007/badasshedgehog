import React, { Component } from "react";
import { View, StyleSheet } from "react-native";
import { useNavigation } from "@react-navigation/native";
import Store from "../../store";
import { Header, Left, Button, Icon, Body, Title, Right } from "native-base";
import vuduStyle from "../../native-base-theme/variables/vuduboss";

class AppHeader extends Component {
  constructor(props) {
    super(props);
  }

  render() {
    const { title, navigation, hasBack } = this.props;
    const { isLoggedIn } = this.context.payload;

    return (
      <>
        <Header>
          <Left style={styles.headerLeft}>
            {hasBack ? (
              <Button
                transparent
                style={styles.backButton}
                onPress={() => navigation.goBack()}
              >
                <Icon name="arrow-back" />
              </Button>
            ) : (
              <View style={styles.backButton} />
            )}
          </Left>
          <Body style={styles.headerBody}>
            <Title style={styles.title}>{title}</Title>
          </Body>
          <Right style={styles.headerRight}>
            {isLoggedIn && (
              <Button transparent onPress={() => navigation.openDrawer()}>
                <Icon name="menu" />
              </Button>
            )}
          </Right>
        </Header>
      </>
    );
  }
}

const styles = StyleSheet.create({
  title: {
    textTransform: "uppercase",
    fontFamily: vuduStyle.fontFamilyExtraBold,
    color: vuduStyle.brandDark
  },
  backButton: {
    width: 50
  },
  headerLeft: {
    flex: 0
  },
  headerBody: {
    flex: 1,
    alignItems: "center"
  },
  headerRight: {
    flex: 0
  }
});

AppHeader.contextType = Store;

export default props => {
  const navigation = useNavigation();
  return <AppHeader {...props} navigation={navigation} />;
};
