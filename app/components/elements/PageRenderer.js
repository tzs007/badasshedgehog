import React, { Component } from "react";
import { StyleSheet, Text } from "react-native";
import { Spinner } from "native-base";
import { storage } from "../../utils/firebase";
import Markdown from "react-native-simple-markdown";
import vuduStyle from "../../native-base-theme/variables/vuduboss";

class PageRenderer extends Component {
  constructor(props) {
    super(props);

    this.state = {
      markdown: null
    };
  }

  /**
   * @async
   * @param {string} slug - page's slug is same as pages/{filename}.md in storage
   * @memberof PageRenderer
   * @returns page content in markdown text
   */
  componentDidMount = async () => {
    const { slug } = this.props;
    const markdown = await storage.getText("pages", `${slug}.md`);

    this.setState({
      markdown
    });
  };

  render() {
    const { markdown } = this.state;
    return markdown ? (
      <Markdown styles={styles}>{markdown}</Markdown>
    ) : (
      <Spinner color={vuduStyle.brandPrimary} />
    );
  }
}

const styles = StyleSheet.create({
  heading1: {
    fontSize: 20,
    fontFamily: vuduStyle.fontFamilyExtraBold
  },
  heading2: {
    fontSize: 18,
    marginBottom: 10,
    fontFamily: vuduStyle.fontFamilyExtraBold
  },
  heading3: {
    fontSize: 16,
    fontFamily: vuduStyle.fontFamilyExtraBold
  },
  link: {
    fontFamily: vuduStyle.fontFamilyExtraBold,
    color: vuduStyle.brandDanger
  },
  mailTo: {
    fontFamily: vuduStyle.fontFamilyExtraBold,
    color: vuduStyle.brandDanger
  },
  text: {
    color: vuduStyle.brandDark,
    fontFamily: vuduStyle.fontFamilyRegular
  },
  plainText: {
    color: vuduStyle.brandDark,
    fontFamily: vuduStyle.fontFamilyRegular
  },
  paragraph: {
    flexWrap: "wrap",
    flexDirection: "row",
    alignItems: "flex-start",
    justifyContent: "flex-start",
    marginBottom: 10
  }
});

export default PageRenderer;
