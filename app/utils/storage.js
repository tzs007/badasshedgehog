// asyncStorage helpers

import { AsyncStorage } from "react-native";

export const _writeAsyncStorage = async (
  asyncStorageKey,
  asyncStorageValue
) => {
  try {
    await AsyncStorage.setItem(`@vudu:${asyncStorageKey}`, asyncStorageValue);
  } catch (error) {
    console.log(error);
    return false;
  }
};

export const _readAsyncStorage = async asyncStorageKey => {
  try {
    const value = await AsyncStorage.getItem(`@vudu:${asyncStorageKey}`);
    if (value !== null) {
      return value;
    }
  } catch (error) {
    console.log(error);
    return false;
  }
};

export const _removeAsyncStorage = async asyncStorageKey => {
  try {
    await AsyncStorage.removeItem(asyncStorageKey);
    return true;
  } catch (error) {
    console.log(error);
    return false;
  }
};

export const _clearAsyncStorage = async () => {
  try {
    await AsyncStorage.clear();
    return true;
  } catch (error) {
    console.log(error);
    return false;
  }
};
