import * as Sentry from "sentry-expo";
import * as firebase from "firebase";
import "firebase/auth";
import "firebase/firestore";
import "firebase/storage";

firebase.initializeApp(Expo.Constants.manifest.extra.firebase);

const auth = firebase.auth();
const db = firebase.firestore();
const firebaseStorage = firebase.storage();
const facebook = new firebase.auth.FacebookAuthProvider();
const firestore = firebase.firestore;

/**
 * Full path for text e.g. pages/about-game.md
 * Full path for image e.g. characters/heisenberg/default.png
 * @param root - first folder in root 'pages' or 'characters'
 * @param slug - second folder e.g. 'heisenberg'
 * @param content - content you need in text or image format
 * @returns text content or
 * @returns image url
 *
 */
const getContent = async (
  root,
  content = null,
  slug = null,
  isImage = false
) => {
  const folder = slug ? `${slug}/` : "";
  const storageRef = firebaseStorage.ref(`${root}/${folder}${content}`);

  try {
    const url = await storageRef.getDownloadURL();
    if (!isImage) {
      const response = await fetch(url);
      const markdown = await response.text();
      return markdown;
    } else {
      return url;
    }
  } catch (error) {
    console.log("error in fetching data", error);
    Sentry.captureException(error);
  }
};

const getImage = async (root, content, slug) => {
  return await getContent(root, content, slug, true);
};

const getText = async (root, slug) => {
  return await getContent(root, slug);
};

const storage = {
  getText,
  getImage
};

export { auth, db, facebook, firestore, storage };
