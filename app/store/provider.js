import React, { Component } from "react";
import Store from "./index";

const StoreProvider = ({ payload, actions, children }) => (
  <Store.Provider
    value={{
      payload,
      actions
    }}
  >
    {children}
  </Store.Provider>
);

export default StoreProvider;
