import React, { Component } from "react";
import { Container, Content, Text } from "native-base";
import MasterLayout from "../components/layouts/Master";
import { AppHeader } from "../components/ui";

const MyRankScreen = () => (
  <MasterLayout>
    <Container>
      <AppHeader hasBack title="My rank" />
      <Content padder>
        <Text>Content goes here...</Text>
      </Content>
    </Container>
  </MasterLayout>
);

export default MyRankScreen;
