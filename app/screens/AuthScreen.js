import React, { Component } from "react";
import { Image, View, StyleSheet } from "react-native";
import { Container, Content, Footer, Button, Text } from "native-base";
import MasterLayout from "../components/layouts/Master";
import { AuthFooter } from "../components/ui";
import Store from "../store";
import vuduStyle from "../native-base-theme/variables/vuduboss";

const logo = require("../assets/images/auth-logo.png");

class AuthScreen extends Component {
  constructor(props) {
    super(props);
  }

  render() {
    const { signIn } = this.context.actions;

    return (
      <MasterLayout>
        <Container style={styles.container}>
          <Content padder contentContainerStyle={styles.content}>
            <View style={styles.brand}>
              <Image source={logo} style={styles.logo} />
              <Text style={styles.catchphrase}>
                Be adorable and prank 'em all!
              </Text>
            </View>
            <View>
              <Button block info rounded large onPress={signIn}>
                <Text style={styles.buttonText}>Facebook login</Text>
              </Button>
            </View>
          </Content>
          <Footer style={styles.footer}>
            <AuthFooter />
          </Footer>
        </Container>
      </MasterLayout>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    backgroundColor: "#fff"
  },
  content: {
    flex: 1,
    justifyContent: "center"
  },
  brand: {
    alignItems: "center"
  },
  logo: {
    width: 228,
    height: 191
  },
  catchphrase: {
    textAlign: "center",
    fontSize: 16,
    marginTop: 20,
    marginBottom: 25,
    fontFamily: vuduStyle.fontFamilyExtraBold
  },
  buttonText: {
    fontSize: 18,
    letterSpacing: 1
  },
  footer: {
    backgroundColor: null
  }
});

AuthScreen.contextType = Store;

export default AuthScreen;
