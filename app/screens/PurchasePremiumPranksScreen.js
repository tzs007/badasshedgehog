import React, { Component } from "react";
import { StyleSheet } from "react-native";
import { Container, Content, Text, Footer, Button } from "native-base";
import MasterLayout from "../components/layouts/Master";
import PageRenderer from "../components/elements/PageRenderer";
import { AppHeader } from "../components/ui";
import Store from "../store";
import vuduStyle from "../native-base-theme/variables/vuduboss";

class PurchasePremiumPranksScreen extends Component {
  _handlePurchase = () => {
    alert("InApp Payment");
  };

  render() {
    const { pages } = this.context.payload;
    const page = pages.find(page => page.slug === "premium-pranks");

    return (
      <MasterLayout>
        <Container>
          <AppHeader hasBack title={page.title} />
          <Content padder>
            <PageRenderer slug={page.slug} />
          </Content>
          <Footer style={styles.footer}>
            <Button block primary rounded large onPress={this._handlePurchase}>
              <Text
                ellipsizeMode="tail"
                numberOfLines={1}
                style={styles.buttonText}
              >
                Purchase
              </Text>
            </Button>
          </Footer>
        </Container>
      </MasterLayout>
    );
  }
}

const styles = StyleSheet.create({
  buttonText: {
    fontSize: 18,
    letterSpacing: 1
  },
  formGroup: {
    paddingHorizontal: 20
  },
  prankeeName: {
    textAlign: "center",
    fontFamily: vuduStyle.fontFamilyExtraBold
  },
  content: {
    flex: 1,
    backgroundColor: "#fff"
  },
  carouselWrapper: {
    flex: 1,
    marginTop: 20
  },
  footer: {
    backgroundColor: "#fff",
    height: 80,
    paddingHorizontal: 20,
    flexDirection: "column",
    elevation: 0,
    shadowOpacity: 0
  }
});

PurchasePremiumPranksScreen.contextType = Store;

export default PurchasePremiumPranksScreen;
