import React, { Component } from "react";
import { View, StyleSheet, Dimensions, Image } from "react-native";
import { Container, Button, Text, Content, Footer, Spinner } from "native-base";
import * as FileSystem from "expo-file-system";
import * as Sharing from "expo-sharing";
import * as Sentry from "sentry-expo";
import { storage } from "../utils/firebase";
import Store from "../store";
import { AppHeader } from "../components/ui";
import MasterLayout from "../components/layouts/Master";
import vuduStyle from "../native-base-theme/variables/vuduboss";

const marginHorizontal = 20 * 2;
const deviceWidth = Dimensions.get("window").width;
const itemWidth = deviceWidth - marginHorizontal; // 40 = marginRight + marginLeft

class PrankShareScreen extends Component {
  constructor(props) {
    super(props);

    this.state = {
      prankedImage: null,
      shareablePrankedImage: null
    };
  }

  async componentDidMount() {
    const { character } = this.context.payload.prankee;
    const { prankedImage, shareablePrankedImage } = await this._getPrankedImage(
      character
    );

    this.setState({
      prankedImage,
      shareablePrankedImage
    });
  }

  _handleShare = async () => {
    const { prankee } = this.context.payload;
    const { shareablePrankedImage } = this.state;

    try {
      const { uri } = await FileSystem.downloadAsync(
        shareablePrankedImage,
        `${FileSystem.documentDirectory}${prankee.name}_pranked.png`
      );
      await Sharing.shareAsync(uri, {
        mimeType: "image/png",
        UTI: "public.png"
      });
    } catch (error) {
      console.log("error");
      Sentry.captureException(error);
    }
  };

  _resetPrankee = () => {
    const { navigate } = this.props.navigation;
    const { resetPrankee } = this.context.actions;
    resetPrankee();
    navigate("WhosGetPrankScreen");
  };

  _getPrankedImage = async prankee => {
    try {
      const { prankType } = this.props.route.params;
      const prankedImage = await storage.getImage(
        "characters",
        `${prankType}.png`,
        prankee
      );
      const shareablePrankedImage = await storage.getImage(
        "characters",
        `${prankType}_pranked.png`,
        prankee
      );

      if (prankedImage && shareablePrankedImage) {
        return {
          prankedImage,
          shareablePrankedImage
        };
      } else {
        console.log("image not found");
      }
    } catch (error) {
      console.log("_getPrankeeImage error:", error);
      Sentry.captureException(error);
    }
  };

  render() {
    const { prankee } = this.context.payload;
    const { prankedImage } = this.state;

    return (
      <MasterLayout>
        <Container>
          <AppHeader title={`${prankee && prankee.name} gets pranked!`} />
          {prankee && (
            <>
              <Content padder contentContainerStyle={styles.content}>
                <View
                  style={{
                    flex: 1
                  }}
                >
                  <View style={styles.prankeeImageWrapper}>
                    {prankedImage ? (
                      <Image
                        source={{
                          uri: prankedImage
                        }}
                        style={styles.prankeeImage}
                      />
                    ) : (
                      <Spinner color={vuduStyle.brandPrimary} />
                    )}
                  </View>
                </View>
              </Content>
              <Footer style={styles.footer}>
                <Button
                  block
                  info
                  rounded
                  large
                  style={{ marginBottom: 10 }}
                  onPress={this._handleShare}
                >
                  <Text style={styles.buttonText}>Share</Text>
                </Button>
                <Button
                  block
                  primary
                  rounded
                  large
                  onPress={this._resetPrankee}
                >
                  <Text style={styles.buttonText}>Prank another one</Text>
                </Button>
              </Footer>
            </>
          )}
        </Container>
      </MasterLayout>
    );
  }
}

const styles = StyleSheet.create({
  buttonText: {
    fontSize: 18,
    letterSpacing: 1
  },
  content: {
    flex: 1,
    backgroundColor: "#fff",
    paddingVertical: 0
  },
  prankeeImageWrapper: {
    backgroundColor: vuduStyle.brandWarning,
    borderRadius: 20,
    overflow: "hidden",
    width: itemWidth,
    flex: 1,
    marginBottom: 10,
    justifyContent: "center",
    alignItems: "center"
  },
  prankeeImage: {
    width: itemWidth,
    height: 600
  },
  footer: {
    backgroundColor: "#fff",
    height: 150,
    paddingHorizontal: 20,
    flexDirection: "column",
    elevation: 0,
    shadowOpacity: 0
  }
});

PrankShareScreen.contextType = Store;

export default PrankShareScreen;
