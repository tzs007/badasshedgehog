import React, { useContext } from "react";
import { Container, Content, Text } from "native-base";
import MasterLayout from "../components/layouts/Master";
import PageRenderer from "../components/elements/PageRenderer";
import { AppHeader } from "../components/ui";
import Store from "../store";

const TermsAndConditionsScreen = () => {
  const { payload } = useContext(Store);
  const page = payload.pages.find(page => page.slug === "terms");

  return (
    <MasterLayout>
      <Container>
        <AppHeader hasBack title={page.title} />
        <Content padder>
          <PageRenderer slug={page.slug} />
        </Content>
      </Container>
    </MasterLayout>
  );
};

export default TermsAndConditionsScreen;
