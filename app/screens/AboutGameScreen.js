import React, { useContext } from "react";
import { Container, Content } from "native-base";
import MasterLayout from "../components/layouts/Master";
import PageRenderer from "../components/elements/PageRenderer";
import { AppHeader } from "../components/ui";
import Store from "../store";

const AboutGameScreen = () => {
  const { payload } = useContext(Store);
  const page = payload.pages.find(page => page.slug === "about-game");

  return (
    <MasterLayout>
      <Container>
        <AppHeader hasBack title={page.title} />
        <Content padder>
          <PageRenderer slug={page.slug} />
        </Content>
      </Container>
    </MasterLayout>
  );
};

export default AboutGameScreen;
