import React, { Component } from "react";
import { View, StyleSheet, Modal, Dimensions } from "react-native";
import { TouchableOpacity } from "react-native-gesture-handler";
import {
  Container,
  Content,
  Thumbnail,
  H3,
  Text,
  Separator,
  ListItem,
  Icon,
  Button
} from "native-base";
import Store from "../store";
import MasterLayout from "../components/layouts/Master";
import { AppHeader } from "../components/ui";
import vuduStyle from "../native-base-theme/variables/vuduboss";

const marginHorizontal = 20 * 2;
const deviceWidth = Dimensions.get("window").width;

const UserInfo = ({ inModal, displayName, email, photoURL }) => (
  <View
    style={[
      styles.userInfo,
      inModal ? { paddingTop: 0, paddingHorizontal: 0 } : null
    ]}
  >
    <Thumbnail
      large={!inModal}
      source={{
        uri: `${photoURL}?height=250`
      }}
      style={styles.userAvatar}
    />
    <View>
      <Text style={styles.userName}>{displayName}</Text>
      <Text note>{email}</Text>
    </View>
  </View>
);

class ProfileScreen extends Component {
  constructor(props) {
    super(props);

    this.state = {
      isModalVisible: false
    };
  }

  _setModalVisible = value => {
    this.setState({
      isModalVisible: value
    });
  };

  _handleDeleteUser = () => {
    const { signOut, deleteProfile } = this.context.actions;
    this._setModalVisible(false);
    deleteProfile();
  };

  render() {
    const { isModalVisible } = this.state;
    const { signOut } = this.context.actions;
    const { user } = this.context.payload;

    return (
      <MasterLayout>
        <Container>
          <AppHeader hasBack title="Profile" />
          <Content>
            <UserInfo {...user} />

            <Separator bordered style={styles.separator}>
              <Text>MANAGE</Text>
            </Separator>
            <TouchableOpacity onPress={this._setModalVisible}>
              <ListItem>
                <Icon
                  type="FontAwesome"
                  name="trash-o"
                  style={styles.profileIcon}
                />
                <Text>Delete profile</Text>
              </ListItem>
            </TouchableOpacity>
            <TouchableOpacity onPress={signOut}>
              <ListItem last>
                <Icon
                  type="FontAwesome"
                  name="power-off"
                  style={styles.profileIcon}
                />
                <Text>Logout</Text>
              </ListItem>
            </TouchableOpacity>
          </Content>
        </Container>

        {/* MODAL */}

        <Modal
          hardwareAccelerated
          animationType="fade"
          presentationStyle="fullScreen"
          transparent={true}
          visible={isModalVisible}
          onRequestClose={() => {
            Alert.alert("Modal has been closed.");
          }}
        >
          <View
            style={{
              flex: 1,
              justifyContent: "center",
              alignItems: "center",
              backgroundColor: "rgba(0, 0, 0, 0.8)",
              backgroundColorOpacity: 0.5
            }}
          >
            <View style={styles.modalCard}>
              <Text style={styles.modalTitle}>
                Do you want to delete your profile?
              </Text>
              <Text style={styles.modalText}>
                This action is permanent and cannot be undone.
              </Text>
              <UserInfo inModal {...user} />

              <View
                style={{
                  flexDirection: "row",
                  justifyContent: "space-between"
                }}
              >
                <Button
                  rounded
                  transparent
                  bordered
                  block
                  dark
                  large
                  onPress={this._setModalVisible}
                  style={{ flex: 1, marginRight: 5 }}
                >
                  <Text>I Do Not</Text>
                </Button>
                <Button
                  rounded
                  block
                  danger
                  large
                  onPress={this._handleDeleteUser}
                  style={{ marginLeft: 5, paddingHorizontal: 20 }}
                >
                  <Text>I do</Text>
                </Button>
              </View>
            </View>
          </View>
        </Modal>
      </MasterLayout>
    );
  }
}

const styles = StyleSheet.create({
  userInfo: {
    flexDirection: "row",
    alignItems: "center",
    padding: 20
  },
  userAvatar: {
    marginRight: 10,
    borderWidth: 5,
    borderColor: vuduStyle.brandPrimary
  },
  userName: {
    fontSize: 18,
    fontFamily: vuduStyle.fontFamilyRegular
  },
  profileIcon: {
    fontSize: 20,
    marginRight: 10
  },
  modalCard: {
    backgroundColor: "#fff",
    width: deviceWidth - marginHorizontal,
    padding: 30,
    borderRadius: 20
  },
  modalTitle: {
    fontFamily: vuduStyle.fontFamilyExtraBold,
    marginBottom: 10
  },
  modalText: {
    marginBottom: 15
  }
});

ProfileScreen.contextType = Store;

export default ProfileScreen;
