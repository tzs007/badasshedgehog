import React, { Component } from "react";
import { View, StyleSheet, Dimensions, Image } from "react-native";
import { Container, Content, Footer } from "native-base";
import Store from "../store";
import { AppHeader } from "../components/ui";
import MasterLayout from "../components/layouts/Master";
import vuduStyle from "../native-base-theme/variables/vuduboss";
import PrankCarousel from "../components/sections/PrankCarousel";

import LottieView from "lottie-react-native";

const marginHorizontal = 20 * 2;
const deviceWidth = Dimensions.get("window").width;
const itemWidth = deviceWidth - marginHorizontal; // 40 = marginRight + marginLeft

class PrankScreen extends Component {
  constructor(props) {
    super(props);

    this.state = {
      lottieView: null
    };
  }

  handlePurchase = () => alert("InApp payment flow here!");

  handlePrank = (src, prankType) => {
    const { navigation } = this.props;

    this.setState(
      {
        lottieView: (
          <LottieView
            autoSize
            ref={animation => {
              this.animation = animation;
            }}
            style={styles.lottie}
            loop={false}
            source={src}
            onAnimationFinish={() =>
              navigation.navigate("PrankShareScreen", { prankType })
            }
          />
        )
      },
      () => this.animation.play()
    );
  };

  render() {
    const { lottieView } = this.state;
    const { prankee } = this.context.payload;

    console.log("prank screen", prankee);

    return (
      <MasterLayout>
        <Container>
          <AppHeader
            hasBack
            title={`Let's prank ${prankee && prankee.name}!`}
          />
          {prankee && (
            <>
              <Content padder contentContainerStyle={styles.content}>
                {lottieView}
                <View
                  style={{
                    flex: 1
                  }}
                >
                  <View style={styles.prankeeImageWrapper}>
                    <Image
                      source={{
                        uri: prankee.defaultImage
                      }}
                      style={styles.prankeeImage}
                    />
                  </View>
                </View>
              </Content>
              <Footer style={styles.footer}>
                <PrankCarousel
                  handlePurchase={this.handlePurchase}
                  handlePrank={this.handlePrank}
                />
              </Footer>
            </>
          )}
        </Container>
      </MasterLayout>
    );
  }
}

const styles = StyleSheet.create({
  buttonText: {
    fontSize: 18,
    letterSpacing: 1
  },
  content: {
    flex: 1,
    backgroundColor: "#fff",
    paddingVertical: 0
  },
  prankeeImageWrapper: {
    backgroundColor: vuduStyle.brandWarning,
    borderRadius: 20,
    overflow: "hidden",
    width: itemWidth,
    marginBottom: 20,
    flex: 1,
    justifyContent: "center",
    alignItems: "center"
  },
  prankeeImage: {
    width: itemWidth,
    height: 600
  },
  lottie: {
    width: deviceWidth,
    height: "100%",
    position: "absolute",
    left: 0,
    top: 0,
    resizeMode: "cover",
    zIndex: 2
  },
  footer: {
    backgroundColor: "#fff",
    height: 120,
    paddingHorizontal: 0,
    flexDirection: "column",
    elevation: 0,
    shadowOpacity: 0
  }
});

PrankScreen.contextType = Store;

export default PrankScreen;
