import React, { useContext } from "react";
import { Container, Content, Text } from "native-base";
import MasterLayout from "../components/layouts/Master";
import PageRenderer from "../components/elements/PageRenderer";
import { AppHeader } from "../components/ui";
import Store from "../store";

const PrivacyPolicyScreen = () => {
  const { payload } = useContext(Store);
  const page = payload.pages.find(page => page.slug === "privacy-policy");

  return (
    <MasterLayout>
      <Container>
        <AppHeader hasBack title={page.title} />
        <Content padder>
          <PageRenderer slug={page.slug} />
        </Content>
      </Container>
    </MasterLayout>
  );
};

export default PrivacyPolicyScreen;
