import React, { Component } from "react";
import Store from "../store";
import { View, KeyboardAvoidingView, StyleSheet } from "react-native";
import {
  Container,
  Button,
  Text,
  Item,
  Input,
  Content,
  Footer
} from "native-base";
import { AppHeader } from "../components/ui";
import MasterLayout from "../components/layouts/Master";
import CharacterCarousel from "../components/sections/CharacterCarousel";
import vuduStyle from "../native-base-theme/variables/vuduboss";

class WhosGetPrankScreen extends Component {
  constructor(props) {
    super(props);
  }

  _setPrankee = () => {
    const { navigation } = this.props;
    const { characters, prankee } = this.context.payload;
    const { setPrankee } = this.context.actions;

    // little bit hacky character reset,
    // if snap carousel didn't snap to new position
    if (!prankee.character) {
      setPrankee(characters[0].name, true);
    }

    navigation.navigate("PrankScreen");
  };

  render() {
    const { prankee, characters } = this.context.payload;
    const { setPrankee } = this.context.actions;

    return (
      characters && (
        <KeyboardAvoidingView
          behavior="height"
          enabled
          keyboardVerticalOffset={0}
          style={styles.content}
        >
          <MasterLayout>
            <Container>
              <AppHeader title="Who's get prank?" />
              <Content contentContainerStyle={styles.content}>
                <View style={styles.formGroup}>
                  <Item rounded>
                    <Input
                      style={styles.prankeeNameInput}
                      placeholder="Prankee's name"
                      onChangeText={text => setPrankee(text)}
                      value={prankee.name}
                    />
                  </Item>
                </View>
                <View style={styles.carouselWrapper}>
                  <CharacterCarousel />
                </View>
              </Content>
              <Footer style={styles.footer}>
                <Button
                  block
                  primary
                  rounded
                  large
                  disabled={!prankee.name}
                  onPress={this._setPrankee}
                >
                  <Text
                    ellipsizeMode="tail"
                    numberOfLines={1}
                    style={styles.buttonText}
                  >
                    {!prankee.name ? "Missing a name!" : "Let's do it!"}
                  </Text>
                </Button>
              </Footer>
            </Container>
          </MasterLayout>
        </KeyboardAvoidingView>
      )
    );
  }
}

const styles = StyleSheet.create({
  buttonText: {
    fontSize: 18,
    letterSpacing: 1
  },
  formGroup: {
    paddingHorizontal: 20,
    marginTop: 25
  },
  prankeeNameInput: {
    textAlign: "center",
    fontFamily: vuduStyle.fontFamilyExtraBold
  },
  content: {
    flex: 1
  },
  carouselWrapper: {
    flex: 1,
    flexDirection: "column",
    paddingTop: 20
  },
  footer: {
    backgroundColor: "#fff",
    height: 100,
    paddingHorizontal: 20,
    flexDirection: "column",
    justifyContent: "center",
    alignItems: "center",
    elevation: 0,
    shadowOpacity: 0
  }
});

WhosGetPrankScreen.contextType = Store;

export default WhosGetPrankScreen;
