import React, { Component } from "react";
import { Text, StyleSheet } from "react-native";
import { NavigationContainer } from "@react-navigation/native";
import { createStackNavigator } from "@react-navigation/stack";
import { createDrawerNavigator } from "@react-navigation/drawer";
import Store from "../store";

import SideBar from "../components/sections/SideBar";

import AuthorizedRoutes from "./components/AuthorizedRoutes";
import UnauthorizedRoutes from "./components/UnauthorizedRoutes";

const Stack = createStackNavigator();
const Drawer = createDrawerNavigator();

class Router extends Component {
  render() {
    const { isLoggedIn } = this.context.payload;

    return (
      <NavigationContainer>
        {isLoggedIn ? (
          <Drawer.Navigator
            headerMode="none"
            initialRouteName="WhosGetPrankScreen"
            drawerStyle={styles.drawer}
            drawerContent={({ navigation }) => (
              <SideBar navigation={navigation} />
            )}
          >
            <>
              <Drawer.Screen
                name="AuthorizedRoutes"
                component={AuthorizedRoutes}
              />
            </>
          </Drawer.Navigator>
        ) : (
          <Stack.Navigator headerMode="none">
            <Stack.Screen
              name="UnauthorizedRoutes"
              component={UnauthorizedRoutes}
            />
          </Stack.Navigator>
        )}
      </NavigationContainer>
    );
  }
}

const styles = StyleSheet.create({
  drawer: {
    backgroundColor: "#fff",
    width: 320
  }
});

Router.contextType = Store;

export default Router;
