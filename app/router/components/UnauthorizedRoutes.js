import React from "react";
import { createStackNavigator } from "@react-navigation/stack";

import AuthScreen from "../../screens/AuthScreen";
import AboutGameScreen from "../../screens/AboutGameScreen";
import TermsAndConditionsScreen from "../../screens/TermsAndConditionsScreen";
import PrivacyPolicyScreen from "../../screens/PrivacyPolicyScreen";

const Stack = createStackNavigator();

const UnauthorizedRoutes = () => (
  <Stack.Navigator headerMode="none" initialRouteName="AuthScreen">
    <Stack.Screen name="AuthScreen" component={AuthScreen} />
    <Stack.Screen name="AboutGameScreen" component={AboutGameScreen} />
    <Stack.Screen
      name="TermsAndConditionsScreen"
      component={TermsAndConditionsScreen}
    />
    <Stack.Screen name="PrivacyPolicyScreen" component={PrivacyPolicyScreen} />
  </Stack.Navigator>
);

export default UnauthorizedRoutes;
