import React from "react";
import { createStackNavigator } from "@react-navigation/stack";

import WhosGetPrankScreen from "../../screens/WhosGetPrankScreen";
import PrankScreen from "../../screens/PrankScreen";
import PrankShareScreen from "../../screens/PrankShareScreen";
import ProfileScreen from "../../screens/ProfileScreen";
import PurchasePremiumPranksScreen from "../../screens/PurchasePremiumPranksScreen";
import MyRankScreen from "../../screens/MyRankScreen";
import AboutGameScreen from "../../screens/AboutGameScreen";
import TermsAndConditionsScreen from "../../screens/TermsAndConditionsScreen";
import PrivacyPolicyScreen from "../../screens/PrivacyPolicyScreen";

const Stack = createStackNavigator();

const navigateWithoutAnimationConfig = {
  animation: "timing",
  config: {
    duration: 0
  }
};

const AuthorizedRoutes = () => (
  <Stack.Navigator headerMode="none" initialRouteName="WhosGetPrankScreen">
    <Stack.Screen name="WhosGetPrankScreen" component={WhosGetPrankScreen} />
    <Stack.Screen name="PrankScreen" component={PrankScreen} />
    <Stack.Screen
      name="PrankShareScreen"
      component={PrankShareScreen}
      options={{
        transitionSpec: {
          open: navigateWithoutAnimationConfig,
          close: navigateWithoutAnimationConfig
        }
      }}
    />
    <Stack.Screen name="ProfileScreen" component={ProfileScreen} />
    <Stack.Screen
      name="PurchasePremiumPranksScreen"
      component={PurchasePremiumPranksScreen}
    />
    <Stack.Screen name="MyRankScreen" component={MyRankScreen} />
    <Stack.Screen name="AboutGameScreen" component={AboutGameScreen} />
    <Stack.Screen
      name="TermsAndConditionsScreen"
      component={TermsAndConditionsScreen}
    />
    <Stack.Screen name="PrivacyPolicyScreen" component={PrivacyPolicyScreen} />
  </Stack.Navigator>
);

export default AuthorizedRoutes;
